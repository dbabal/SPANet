import onnxruntime as ort
import torch
import numpy as np
from argparse import ArgumentParser
import os, uproot, ROOT
import pandas
from array import array
from onnx_prediction_selection import extract_predictions
from time import time

# Script responsible for injecting spanet predictions into root files. It starts with loading trees from root files into which prediction should
# should be injected. Tree is loaded to dataframe and input for spanet model is created by transforming vectors
# of jet pt, eta, phi and m (mass is calcuated from these values) into size 20 vectors (0 are added for missing jets).
# Then the input vectors are passed to onnx runtime session where the model is inferred. Predicted indexes of light 
# jets and b jet for first and second top are finally saved into original root files. Note that original files are not affected
# because all trees (together with predictions) are copied into new root files.
# Command line arguments:
#  -rf : root file name
#  -i  : absolute path to folder where root files are stored
#  -o  : absolute path to folder where transformed root files (with predictions) should be stored
#  -m  : absolute path to onnx model files used for inferrence

BTAG_BIN = 3 # corresponds to 77% btag WP
MAX_JET = 20
BATCH_SIZE = 128

def load_DF_from_file(root_file, tree_name):
    # load events from root file and store them in dataframe
    events = uproot.open(root_file)[tree_name]
    
    df_vars = ["eventNumber","mcChannelNumber","jet_pt","jet_eta","jet_phi","jet_e","jet_tagWeightBin_DL1r_Continuous"]
    input_np = events.arrays(df_vars,library="np")
    df_root = pandas.DataFrame(input_np)

    # need to tranform pt and E from MeV to GeV (needed by spanet model)
    df_root['jet_pt'] = 0.001*df_root['jet_pt']
    df_root['jet_e'] = 0.001*df_root['jet_e']
    a=time()
    # transform vectors with size N_jet to fixed size 20 and calculate jet masks, btag bools and jet masses
    df_root[['jet_pt', 'jet_eta', 'jet_phi', 'jet_m', 'jet_mask', 'is_btagged']] = df_root.apply(transform_DF, axis=1, result_type="expand")
    b=time()
    print("transform: ", b-a)
    return df_root

def array_to_list(object):
    # function for transforming all numpy arrays (also underlying) to lists
    if type(object) == np.ndarray:
        object = object.tolist()
    if type(object) == list:
        object = [array_to_list(x) for x in object]
    return object

def prepare_spanet_input_data(df):
    df_jet_prop = df[['jet_pt', 'jet_eta', 'jet_phi', 'jet_m', 'is_btagged']]
    df_jet_mask = df[['jet_mask']]

    # store jet properties to numpy array, need to transform list of np. arrays to nD np array to perform transpose
    # the structure of the resulting array is [[[1st jet pt,eta,phi,m,btag],[2nd jet pt, eta, phi, m, btag],...],next event...]
    # and for mask structure is preserved [[1st event],[2nd event],...]
    jet_prop_array = df_jet_prop.to_numpy().tolist()
    jet_prop_array = np.stack(np.asarray(jet_prop_array))
    jet_prop_array = np.transpose(jet_prop_array,(0,2,1))
    jet_prop_array = [list(jet_prop_array[x:x+BATCH_SIZE]) for x in range(0, len(jet_prop_array), BATCH_SIZE)]
    jet_prop_array = array_to_list(jet_prop_array)
    
    jet_prop_array = [list(x) for x in jet_prop_array]
    jet_mask_array = df_jet_mask.to_numpy().tolist()
    jet_mask_array = [x[0] for x in jet_mask_array]
    jet_mask_array = np.asarray(jet_mask_array)
    jet_mask_array = [list(jet_mask_array[x:x+BATCH_SIZE]) for x in range(0, len(jet_mask_array), BATCH_SIZE)]
    jet_mask_array = array_to_list(jet_mask_array)

    return jet_prop_array, jet_mask_array

def transform_DF(df):
    # calculate mass from pt, eta, phi, e, add jet mask (if jet is present) and btag bools
    m_array = []
    jet_mask_array = []
    btag_mask_array = []
    length = len(df['jet_pt'])
    for i in range(length):
        lor = ROOT.TLorentzVector()
        lor.SetPtEtaPhiE(df['jet_pt'][i], df['jet_eta'][i], df['jet_phi'][i], df['jet_e'][i])
        m_array.append(lor.Mag())
        jet_mask_array.append(True)
        # extract btagging information
        if df['jet_tagWeightBin_DL1r_Continuous'][i] >= BTAG_BIN:
            btag_mask_array.append(1.0)
        else:
            btag_mask_array.append(0.0)

    # add zeros to the end of arrays (required by spanet) - to make length 20
    num_zeros = MAX_JET-length
    m_array = np.pad(np.asarray(m_array), (0,num_zeros), 'constant')
    btag_mask_array = np.pad(np.asarray(btag_mask_array), (0,num_zeros), 'constant')
    jet_mask_array = np.pad(np.asarray(jet_mask_array), (False,num_zeros), 'constant')
    jet_pt = np.pad(df['jet_pt'], (0,num_zeros), 'constant')
    jet_eta = np.pad(df['jet_eta'], (0,num_zeros), 'constant')
    jet_phi = np.pad(df['jet_phi'], (0,num_zeros), 'constant')

    return jet_pt, jet_eta, jet_phi, m_array, jet_mask_array, btag_mask_array

def predict_with_onnxruntime(sess, trim, *inputs):
    names = [i.name for i in sess.get_inputs()]
    dinputs = {name: input for name, input in zip(names, inputs)}

    # run the inferrence session for BATCH_SIZE events
    res = sess.run(None, dinputs)
    
    names = [o.name for o in sess.get_outputs()]
    prediction = [torch.autograd.Variable(torch.FloatTensor(res[0])), torch.autograd.Variable(torch.FloatTensor(res[3]))]
    
    # extract predictions according to extract_predictions method from spanet package (finding peaks corresponding)
    # to top and antitop in loss space (20x20x20)
    extracted_pred, values = extract_predictions(prediction)

    # cut out dummy events (in case of last batch which has less than BATCH_SIZE events)
    if trim > 0:
        extracted_pred = [pred[0:trim] for pred in extracted_pred]
        values = [val[0:trim] for val in values]
    
    return extracted_pred, values

def predict_for_tree(root_file, tree, path_to_model):
    a = time()
    df = load_DF_from_file(root_file, tree)
    b = time()
    jet_prop, jet_mask = prepare_spanet_input_data(df)
    c = time()
    full_predictions = []
    full_values = []
    sess = ort.InferenceSession(path_to_model)
    for data, mask in zip(jet_prop, jet_mask):
        trim = -100
        # special treatment for last batch which has <128 events (need to add dummy events for all inputs, zeros
        # for jet properties and False booleans for masks)
        if len(data) != BATCH_SIZE:
            trim = len(data)     
            add = BATCH_SIZE-len(data)     

            data_add = np.zeros((add,20,5)).astype('f')
            data_concat = np.concatenate((np.asarray(data), data_add), axis=0)
            data = array_to_list(data_concat)

            mask_add = np.full((add,20), False)
            mask_concat = np.concatenate((np.asarray(mask), mask_add), axis=0)
            mask = array_to_list(mask_concat)
        predictions, values = predict_with_onnxruntime(sess, trim, data, mask)
        full_predictions.append(predictions)
        full_values.append(values)
    d = time()
    print("loading df: ",b-a)
    print("preparing inputs: ",c-b)
    print("prediction: ",d-c)
    print("-----------------------")
    full_predictions = list(map(np.concatenate, zip(*full_predictions)))
    full_values = list(map(np.concatenate, zip(*full_values)))
    return full_predictions, full_values

def pop_all(arr):
    length = len(arr)
    for i in range(length):
        arr.pop(0)

def replace_array_elements(arr1, arr2):
    # function internally needed when filling tree branch
    for i in range(0,len(arr1)):
        arr1[i]=arr2[i]

def do_injection(fname, input_folder, output_folder, path_to_model):
    # Create a new root file
    input_root_file_path = input_folder + "/" + fname
    File = ROOT.TFile(input_root_file_path)
    if not os.path.isdir(os.path.abspath(output_folder)): os.mkdir(os.path.abspath(output_folder))
    NewFile = ROOT.TFile(input_root_file_path.replace(os.path.abspath(input_folder), os.path.abspath(output_folder)),"recreate")
    # Loop over trees in root file
    keys = File.GetListOfKeys()
    Treenames = []
    a=time()
    for key in keys:
        if(key.GetClassName() != "TTree"): # ignore TObjects that are not TTrees
            dir = ROOT.TDirectoryFile(key.ReadObj().GetName(), key.ReadObj().GetName())
            keys_dir = key.ReadObj().GetListOfKeys()
            dir.cd()
            for k in keys_dir:
                obj = key.ReadObj().Get(k.GetName())
                dir.Add(obj)
            NewFile.Write()
            del dir
            continue
        elif(key.ReadObj().GetName() == ""): # ignore TTrees that do not have a name
            continue
        else:
            Treenames.append(key.ReadObj().GetName())
    b=time()
    print("copying stuff: ", b-a)
    for tName in Treenames:
        Tree = File.Get(tName)
        print(tName)
        # black listing a few trees which are simply copied
        if tName in ["", "nominal_Loose", "truth", "particleLevel", "sumWeights", "PDFsumWeights", "AnalysisTracking"]:
            NewTree = Tree.CloneTree()
            NewFile.Write()
            continue
        else:
            NewTree = Tree.CloneTree(0)
        
        
        c=time()
        NewBranchesArray = {}
        NewBranchesFloat = {}
        NewBranchesArray["top1_predictions"] = array('i',[0,0,0])
        NewBranchesArray["top2_predictions"] = array('i',[0,0,0])
        NewBranchesFloat["top1_peak_values"] = array('f',[0])
        NewBranchesFloat["top2_peak_values"] = array('f',[0])
        for BranchName, NewBranch in NewBranchesArray.items():
            NewTree.Branch(BranchName, NewBranch, BranchName+"[3]/I")
        for BranchName, NewBranch in NewBranchesFloat.items():
            NewTree.Branch(BranchName, NewBranch, BranchName+"/F")

        nentries = Tree.GetEntries()
        if nentries == 0:
            NewFile.Write()
            continue

        predictions, values = predict_for_tree(input_root_file_path, tName, path_to_model)

        for entry_number in range(0,Tree.GetEntries()):
            Tree.GetEntry(entry_number)
            replace_array_elements(NewBranchesArray["top1_predictions"],predictions[0][entry_number])
            replace_array_elements(NewBranchesArray["top2_predictions"],predictions[1][entry_number])
            NewBranchesFloat["top1_peak_values"][0] = values[0][entry_number]
            NewBranchesFloat["top2_peak_values"][0] = values[1][entry_number]
            NewTree.Fill()
        NewFile.Write()
        d=time()
        print("saving tree: ",d-c)
    NewFile.Close()

parser = ArgumentParser()
parser.add_argument("-rf", "--fname", type=str,
                    help="Name of root file to which prediction should be injected.")

parser.add_argument("-i", "--input_folder", type=str,
                    help="Name of the input folder containing root file.")

parser.add_argument("-o", "--output_folder", type=str,
                    help="Name of the output folder where new root file will be stored.")

parser.add_argument("-m", "--path_to_model", type=str,
                    help="Path to the .onnx file with pretrained model.")

arguments = parser.parse_args()
do_injection(**arguments.__dict__)
# do_injection("364128.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CVetoBVeto.e5307_s3126_r10724_p4512_output.root", "/misc/atlas23/babal4/Ntuples/2LOS_11_12_merged", "/misc/atlas23/babal4/Ntuples", "./model_ttbar_full.onnx")
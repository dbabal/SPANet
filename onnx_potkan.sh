#!/bin/sh
pwd
basedir="/home/b/babal4/spanet_git/SPANet"
inputdir="/misc/atlas23/babal4/Ntuples/2LOS_11_12_merged_tt"
outputdir="/misc/atlas23/babal4/Ntuples/2LOS_11_12_merged_tt/with_spanet"
modelfile="/home/b/babal4/spanet_git/SPANet/ttZ_model_6j1b_combined.onnx"
outdir="/home/b/babal4/spanet_git/SPANet/out"
errdir="/home/b/babal4/spanet_git/SPANet/err"
echo "Running onnx prediction for ", $1
start=$(date +"%T") 
echo "Start time : $start"
cd ${basedir}
source ./setup_onnx.sh
python3 inject_onnx.py -rf $1 -i ${inputdir} -o ${outputdir} -m ${modelfile}
mv onnx_*.e ${errdir}
mv onnx_*.o ${outdir}
echo "End of run"
end=$(date +"%T") 
echo "End time : $end"
from typing import Optional
from argparse import ArgumentParser
from onnx2keras import onnx_to_keras

import numpy as np
import torch
from spanet.evaluation import load_model

def main(log_directory: str, test_file: str, out_file: str):

    model = load_model(log_directory, test_file)

    input_np = np.random.uniform(0, 1, (128,20,5))
    input_mask = np.random.uniform(0, 1, (128,20))>0.5
    input_var = torch.autograd.Variable(torch.FloatTensor(input_np))
    input_var_mask = torch.autograd.Variable(torch.BoolTensor(input_mask))
    
    args = [input_var,input_var_mask]

    if not isinstance(args, list):
        args = [args]

    args = tuple(args)

    dummy_output = model(*args)

    if isinstance(dummy_output, torch.autograd.Variable):
        dummy_output = [dummy_output]

    input_names = ['input_{0}'.format(i) for i in range(len(args))]
    output_names = ['output_{0}'.format(i) for i in range(len(dummy_output))]

    torch.onnx.export(model, args, out_file, input_names=input_names, output_names=output_names, opset_version=12)

if __name__ == '__main__':
    parser = ArgumentParser()

    parser.add_argument("log_directory", type=str,
                        help="Pytorch Lightning Log directory containing the checkpoint and options file.")

    parser.add_argument("-tf", "--test_file", type=str,
                        help="Replace the test file in the options with a custom one. "
                             "Must provide if options does not define a test file.")

    parser.add_argument("-o", "--out_file", type=str, default="my_model_onnx.onnx",
                        help="Name of the output file in which keras model will be stored")

    arguments = parser.parse_args()
    main(**arguments.__dict__)


